作者:我尝试对迷宫篇为止的主要的登场人物进行了一下总结,以便供大家参考。

登场人物“等”登场人物“等”一边说登场人物只登载着没有察觉到了其他的设定也预先登载了

---

◆主人公一侧

如月　诚一

（主人公）男

高中二年级，重度妹控。【译：可是你的妹妹已经把你忘了】

和平时一样来到学校，被神从学校放飞到异世界的其中一人。
平凡的高中生（自称）。

因隐性宅的关係拥有大量无用的有关异世界知识。【译：毕竟到异世界前有这些知识也没用…】


镰仓　结衣
女

如月的同班同学，游泳部（部员）。

暗恋着诚一。
但是，其行为举止不断升级，逐渐扩大与跟踪狂无益的行为，直到夜袭（未遂）为止发展。
变态。【译：变态是作者说的…表打我】


二宫　泰牙
男

男主的同班同学，喜欢结衣。
有着在结衣哪裡安装窃听器的胆量【译：不知道是已遂还是未遂】，也拥有跟踪狂的气质。
嫉妒者男主，经常找男主的茬。（这个那个地顶嘴）【译：为啥他不是变态！！！！因为是笨蛋？？】


浅野　亮
男

男主的同班同学，成绩优秀，运动万能，在学校能够收到大量情书水平的帅哥。【译：介绍好少，因为是帅哥吗还是说是因为是勇者（笑）】


神田　铃
女

男主的同班同学，结衣的青梅竹马。
无表情角色，但表情起伏很明显（无表情的角色并不是说感情的起伏匮乏），顺便说一下铃是个缺乏胸部起伏的贫乳。
可以考虑鬼畜。【译：（结构考えが鬼畜）应该是这麼翻译吧~~~】

为了吧诚一和如月撮合到一起被贴上了多管闲事的标签。
因结衣的事和诚一很亲近。


顺便说一下，不过作者喜欢不想被诚一拿走,不过陷於了为了更换铃和结衣的位置这样的神秘的行动。

【译：这裡不明白求解: （ちなみに出番は少ないが作者のお気に入りで诚

一に取られたく无いがために铃と结衣のポジションを入れ替えるという谜の行动に走った。）难道她才是女主，还是两人都收？？？】


野本　大地
男

男主的同班同学, 从高中一年级开始的朋友。
身为诚一的好友试验前总是为诚一求情??【译：…】


如月　荵
女

男主的妹妹,初中二年级。
从年幼的时候开始被主人公帮忙。相当的兄控。
还没有达到头戴着裤子或窥视洗澡程度的变态。
拥有这非常温柔的心灵但是哥哥限定。
现在地球上被消除与诚一有关的记忆活着。【译：妹妹是兄控，但是哥哥的记忆消失了，你们说会怎麼样】


◆异世界一侧

玛丽•nosutia
女

诺斯提亚王国的第二公主,勇者召唤的罪魁祸首。【译：我想知道第一公主是谁】

与勇者相见的时候谨慎认真的对应,但实际上是非常傲慢的性格。
对亮一见钟情并给予优惠的同时,因社会地位低的事而被男主蔑视。


高卢•诺斯提亚
男

诺斯提亚王国的国王, 玛丽的糊塗家长。【译：音译就是笨蛋家长】

召唤勇者的王。
谨慎言行的背後不知道隐藏着什麼阴谋………?


丹
男

诺斯提亚王国的骑士团长,负责教授勇者剑术。
平时是个豪爽的人,是个接受诚一諮询的亲切的人。


serisu
女

诺斯提亚王国的宫廷魔导师,负责教授勇者魔法。
被王女灌入不少诚一的坏事,所以歧视诚一。


◆其他

史莱姆
性别不明

诚一用史莱姆果冻和史莱姆核运用使役制成的史莱姆，与诚一相当接近。
只是从魔不知不觉因读者的爱成为了女主。【译;所以说是萌王啦】


伯里亚（独眼巨人）

性别不明，女的吗？

男主在迷宫中得到的从魔，虽然是很短的时间,但非常接近诚一。
在与奇美拉的战鬥中死亡。


魔神桑納托斯
性别不明，男人吗？

撒鲁洛斯的迷宫的核心兼迷宫主.好像和以前古代神的一个人一样厲害(本人谈)【译：还不如说是自称那】


神(？)　　性别不明、男？

把诚一以及其同学放飞到异世界的罪魁祸首.关於男主的力量掌握着什麼秘密……?【译：从第5話来看应该和男主认识】


◆状态

除了一些没有等级的技能外有着Lv1～10的10个等级
Lv1，2是初学者，只知道基础技能的使用方法的水平。
Lv 3是中级者水平，Lv4内行水平,也就是可以独当一面的水平。
Lv5上级水平，到达这个等级的人根據技能的不同领域在吃下去。
Lv6达人水平,拥有到达这个等级剑术技能的有耐性的人可以开设道场。
Lv7,Lv8是成为王国的近卫骑士水平,预测丹的剑术等级就在这裡。
Lv9是勇者传说中出现勇者的一行人和魔王的水平, 以外的没被确认。
Lv10被称为神的水平,只在神話中出现过。


◆关於〇称号

主要是持有者的行动来取得地位和其他补正。
例如被召唤的同学都拥有着[跨过异世界的人]的称号。


◆关於魔法

魔法附有一定的排位.如下

　初级
　下级
　中级
　上级
　特级
　圣霊级
　王级
　帝级
　神级
　无限级

顺便一提这个级别魔法是本身的强度,初级的魔术『水球』是孩子能够杀死虫子的程度。另外，把神制作的这个星球一击消灭的oudaboulu也属於初级魔法。
另外，原创制作的魔法是严格的等级是不能用○○级相当，像这样被判断。


◆关於武器防具

武器防具也有一定的等级如下
下级

　中级
　上级
　特级
　特异(ユニーク)级
　伝说(レジェンド)级
　神器(ゴッズ)级
　无限级

而且，这裡的等级与魔法不同是自动定的。拥有一定的鉴定技能能知道排位。
