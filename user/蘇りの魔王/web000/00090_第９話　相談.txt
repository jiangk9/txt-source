并不是就不能硬是把古蓝叫起来，只是鲁鲁对强自叫醒因受了巨大冲击而倒下的他感到畏缩，而且鲁鲁觉得有必要和伊莉丝进行某种程度的交谈，他已经决定把古蓝直到苏醒为止的接下来这段时间用於询问。
原本，他就在意着。
为什麼，伊莉丝会在这种遗迹中进入单人長期睡眠装置中睡眠。
而且，还有魔王鲁鲁斯利阿＝诺鲁多被勇者打倒以後，同胞们到底都变得怎麼样了的事情也是。
鲁鲁为了听取这两方面的答案，让差不多哭尽了的伊莉丝冷静下来，并让她在地面上坐下，他则是坐到她的旁边开口询问。

【然後……我有想要问你的事，能回答我吗？】

对着这麼说的鲁鲁，伊莉丝也是一副当然的，模样点头回答。

【当然的！无论是什麼，请尽管吩咐。不过……我也有想要问您的事情。如果可以的話，您能为我解答吗？】

经她这麼一说，鲁鲁“啊啊，也是啊”的想到。
就伊莉丝而言，尽管说已经确信了鲁鲁就是魔王，但他的姿态却是人族的姿态，而且甚至连他的外表都改变了，年龄也是孩子，这令伊莉丝处在了完全弄不明白的状态中。
也许关於这方面的情况还是先说明比较好。
如此想的鲁鲁颔首点头。

【啊啊，当然的。在伊莉丝看来，我是否是真的魔王斯利阿＝诺鲁多还是个疑问啊……】

以现实来考虑，即使在这一点上被怀疑也并不出奇，所以鲁鲁才会这麼说。
然而，伊莉丝却摇头。

【不！叔叔（ojisama）就是叔叔（ojisama）！没有错。这一点上，我没有怀疑。】

本应该退去的泪水开始在他的眼角如珠玉般的积蓄。鲁鲁面对如此说着的伊莉丝所汹汹涌动的情绪也是惊讶不已。
被她信任到这种地步，不管怎麼说，他都没有想到过。
因为根本上，如果是考虑存在怀疑这种可能性的程度的話，他也会认为没什麼，所以鲁鲁对於她所说的那句等同於没有夹杂丝毫怀疑的話语感到惊愕。

【虽然很高兴……只是，为什麼？】
【那是因为，那个魔力弹小布袋。叔叔（ojisama）也知道怎麼做吧？驾驭那种数量的魔力弹，无论是怎麼样的魔族实力者，能做到的也只有叔叔而已。】
【确实我心裡虽然觉得你能记起来太好了啦……确信这种程度？如果是伊莉丝的父亲……帕卡斯的話不是能做到吗？】

尽管说鲁鲁在众多魔族中最为擅长魔力的操控，不过现在这个身体还做不到那种程度。
也有还未习惯的因素在，毕竟和过去魔王时候的身体性能不同。
魔力弹的操作是把魔力放于体外的操作技巧，而且伊莉丝所放出的魔力正是正因为是魔族之物，鲁鲁仅仅是以接近过去魔王时的感觉操纵而已。如果要鲁鲁自己一个人以同等的规模把魔力弹制作成小沙包的話，不能做到的可能性很高。
而且，如果是过去的盟友们的話，那种程度也不是就做不到，鲁鲁是这麼感觉。
然而，伊莉丝却摇头

【不，父亲是做不到的。该怎麼说呢……那个人不并不擅长魔法的精细驾驭，虽然也不是极度不擅长，但魔力弹的精密操作，他是做不出来的。我还有和他一起玩过许多次遊戏的记忆，他至多做到十个的程度就已经到极限了。像叔叔一（ojisama）一样操控数百个……是不可能的】

经她这麼一说的話，鲁鲁倒也记得伊莉丝的父亲帕卡斯＝达尔斯罗拉，稍微有些粗率的地方，对那种精修的作业也不怎麼拿手。
在战鬥面上帕卡斯是一个极为优秀的男子，有着能够以一人敌一军程度的突出力量，不过鲁鲁没有见过他有进行过魔力緻密驾驭的时候。
付诸于庞大魔力的大规模魔法，以及使之通行於身，以自己的肉体战鬥的肉弹之男，魔力操作什麼的，或许基本上都没有使用过。

【确实，那傢伙也不是不知道……也还有其他人吧。像是缪多斯像是雷努】

不管哪一个都是鲁鲁过去的部下，长于魔法魔术的砖家的名字。
缪多斯是乾巴巴且神经质的魔导师，雷努是超喜欢破坏，除了超常力量就是超常魔力的女性。
然而伊莉丝还是摇头。

【虽然我觉得没有缪多斯爷爷做不到的事情，根本上来说，他是一个如果是去做魔力弹沙包这种程度的事情的話，更想把时间用在其它方面的人，雷努姐姐该说是有些三分钟热情………像这样，注意力涣散………】

并不是说做不到，而是在性格上而言不会去做吗。
伊莉丝所说的話，他也非常清楚。
鲁鲁记起来了。
缪多斯，如果要从哪方面来说他的話，那就是学者气息的研究笨蛋，雷努则是会放出华丽魔法的呆子。
然後，令他惊异的是，过於的怀念感令得他的涙腺一瞬间湿润起来。

没想到光是这种平平无奇的話题，就已经让泪水快要溢了出来。
不管是缪多斯还是雷努，尽管当时胡闹，自己也明明总是冷眼看待，然而，现在却意识到了，自己似乎很喜欢他们。
注意到鲁鲁这副模样的伊莉丝，担心起他来，於是说道。

【非常抱歉………对於中途倒下的叔叔您而言，这是个艰辛的話题】
【不……没事的。倒不如说，能够谈论充满怀念的話题反而更高兴哦。不过，原来如此啊。照伊莉丝的話来说，如果是这种事的話，能做到的应该也就只有我了吧】

对重新打起精神继续話题的鲁鲁，伊莉丝也跟着配合。

【嗯嗯，就是这样。但，还有不可思议的地方】
【是什麼？】
【叔叔（ojisama）是叔叔（ojisama）这一点没有错，可是您的身体到底是怎麼回事。不会有错，那是人族的身体。而且您的年龄也………这麼说的話，该怎麼形容，

和我一样的您的年龄样子。到底是怎麼变成那样的………让我忍不住感到很深的好奇】
【啊，也是啊，刚刚说过發生了各种各样的事】
【这麼说的話，您刚刚也确实说过呢。那又是怎麼……？】

面对疑惑的伊莉丝，鲁鲁思考着该如何进行说明才好。
然而，这也不是相当複杂的話题。
如果是对着人族的話，也许会需要相当精细的说明，不过若是对着魔族的話，鲁鲁想起了，在魔族中即使是通过一句話去理解亦是时有有之事。
所以，他如此说道。

【很简单的事。……伊莉丝。我体验了轮回转生哦】

然後，伊莉丝瞪大了眼睛，向鲁鲁展示出了惊愕。
然而，她的这份惊异绝非是听到了不可能之事的那种感觉，倒不如说是近乎于突然耳闻了喜讯时的模样。
轮回转生是，魔族的信仰。
如此般所言，世界是循环，生命被承继，向着下一个世代不断发展。
正因为如此相信着，魔族才能持有坚强的心战鬥。
那之後，伊莉丝缓缓的吸了口气，问道。

【也就是说，叔叔您死去後，经过了某种程度的日月後被转生成了人族，总而言之就是那样的吗？】

能够很快理解，真是帮大忙了，鲁鲁微笑了出来。
如果是对着人族的話，这样应该是行不通的吧。
因为，过去轮回的观点对於人族而言，与其说是异端，倒不如说基本处在想象之外。
把那个观点逐一说明，不管怎麼说都很麻烦。
生物如果死了的話，就会就此终结。
这就是人族的观点。
鲁鲁对伊莉丝的问题，进行了回答。

【就是那样。现在的我，正是人族哦。也就是说，有着人族的父亲和母亲。他们不管哪一个都是很好的人……很温柔的人哦】

顾虑到也许会有来自伊莉丝排斥的情况，鲁鲁才这麼说出来。
虽说伊莉丝不是憎恨人族的类型，不过毕竟是进行种族战争的战争对手的种族。
无法抱有好意，也是可能的。
原本，刚起来的伊莉丝就对人族感到过厌恶。
然而，从伊莉丝的嘴中出来的却是令人意外的台词。

【听了您的这句話……该怎麼形容呢。总之安心了】
【那又是，为什麼】

鲁鲁疑惑的提出了疑问。

【恐怕是……………听了叔叔（ojisama）转生的事，还有对和人族的生活感到满足的情况。也因此，方才稍稍持有的偏颇的看法被洗掉了，……huhu】

伊莉丝突然微笑了出来。
怎麼了，鲁鲁感到了疑问，伊莉丝继续说道。

【不……我，刚刚虽然已经说过了，叔叔您死去後，我一直把心血倾注在报仇上。相当的乱来。】

明显的，鲁鲁感觉时间似乎停止了。
有着素雅的美丽银髮和态度的眼前的少女伊莉丝，“乱来”到底是……鲁鲁如此想，
或许是读取了鲁鲁的这种表情，伊莉丝开始慌慌张张的摇头辩解起来。
也许那是因为不想被鲁鲁讨厌。

【不！不！不是的……那个，即使说是乱来……那个……也没有做出多过分的事情来哦。只是稍稍率领品格不好的魔族，做出了一些像是强袭了人族的军队使之毁灭，潜入人族军的帐篷在将兵的枕头下留下『总有一天会杀掉你，给我做好觉悟』的手书之类的，基本就这种程度而已！】

这些已经足够了哦………虽然鲁鲁是这麼想，不过察觉到了不应该在这裡进行多余指摘的鲁鲁决定了改变話题。

【嘛，那个話题就算了吧。話虽如此，我死了之後也能做到那种事，也就是说魔族没有被灭亡吗？如果按照那个情形的話，我想魔族即使会在某时某刻被大规模狩猎也不奇怪………？】
【啊啊……也是呢。叔叔您也已经不在了呢……对，有过魔族狩猎。确实那已经是不幸的时代。教会派遣神聖騎士们慢慢的把魔族逼到走投无路，魔族数量也逐渐的减少了。那个如果一直继续的話，就算是魔族，也会被根绝掉吧。】
【如果一直继续的話？难道没有继续下去吗？】

这还真是令人意外。
如此憎恨着魔族而统一了的人族，居然没有对他所憎恨的魔族继续狩杀，这事。
伊莉丝继续道。

【与其说没有继续下去，倒不如说变得没法继续下去吧。人族的势力在在叔叔（ojisama）逝去後不久，被一分为二了。也就是说，他们内部开始了分裂。照父亲的说法就是，因为魔王这一明确的威胁已经没有了，不过现实实际上就是那样的吧……。一方的旗帜是教会，然後另一方是，给与叔叔致命一击的……】
【………勇者吗】

这是他所考虑过的情形。
在最後，鲁鲁播下了种子。
那，结出了果实吧。
尽管不知这是好还是坏，不过，那个勇者，确实忠实的遵守了约定。
然而，其结果就是，人族一分为二。

【欸欸，就是这样。真是讽刺。刺杀了魔族之王的人，拯救了魔族的命运。然後那之後的事……实际上，我也不知道。还不知道过了多少年，现在到底又变得怎麼样了呢。我很是在意】

那麼，最後是，变得怎麼样了，到了这种最令人在意的地方，伊莉丝如此说着摇头。
鲁鲁，惊异，疑问。

【………这又是为什麼？】
【我之前说过曾经率领品格不好的魔族袭击人类的，不过某一天，那时我不知道被谁给抓住了……意识到时，已经在不清不楚的某个场所的床上，一阵难以忍受的睡意向我袭来。虽然在即将睡着前察觉到了，那是由长期睡眠装置造成的睡意，然而已经晚了。之後我就那麼沉入了睡眠，醒来时眼前出现的是叔叔。现在想来的話，那是因为在这裡吧。】
【不是你自己进入长期睡眠的？】

那是并不怎麼好的話题。
从伊莉丝那裡能得到的情报，在中途断掉了。
伊莉丝好想对自己被抓的事情很不满，稍微有些尖锐的继续说道。内容，转移到抓捕伊莉丝人的特徵的話题上。

【欸欸。……总觉得，真是个弄不清楚的人。身上穿着一身法衣，或许是因为风帽戴的也深的缘故，连面貌也没有露出来。恐怕她的身上连認識阻害系的魔導具也有吧。即使注视也基本探知不了她……不过，从体型和香气来看，恐怕是女人，这一点倒是知道……】

光是这个的話，什麼也无法知道。
那只不过是一般的魔術師・魔導師的装束而已，因为，要是女性魔術師・魔導師的話，多如繁星。
然而，盯上魔族并且无伤捕获的手法的高明，让人预想到相当的纯熟度。
那个正体让人在意。那个目的也是。

【这样啊……但是到底是谁？　而且还盯上伊莉丝，特意让你长期睡眠什麼的，是为了什麼？】
【这正是我想要问的。但是………现在的話我很感谢】

忽然，伊莉丝的声音和表情柔和了下来，微笑着朝向鲁鲁，注视着他。
这是怎麼了，鲁鲁感到了疑惑。

【为什麼】
【托此之福，见到了叔叔】

充满了深情的那个声音。
她似乎是真的很欢喜，自己过去对她做了很抱歉的事情，鲁鲁感到了深刻的後悔之念。

【………伊莉丝】

对着叫出名字的鲁鲁，伊莉丝似是恳求的说道。

【叔叔请您，已经不要再像那样死去了………请不要再那样做。这是魔族全体的愿望】

是在说在他和勇者们的战鬥中，以一个人挑战的事吧。
由於其他之者不得不分配到和军队以及人族强者的战争中去，除了那麼做之外别无它法，不过现在来想的話或许会有更好的做法。
所以，鲁鲁很老实的道歉了。

【……抱歉了啦……嘛，这一回是人族。应该不会發生同样的事吧】

【……是，这样呐。这样的話好高兴。可是……叔叔变成人族後，稍微有点问题】

【什麼问题？】

【我要以什麼样的身份在叔叔您的身边……？】

理所当然般的被宣言了呆在自己的身边，该如何回答，一瞬间鲁鲁语塞了。
不过把伊莉丝放置在身边的决定，从在长期睡眠装置的床上发现她开始就已经在鲁鲁的心中决定了下来。
所以，立刻切换了过来，在那个方法方面，转移了話题。

【怎麼说呢，现在必须先从关於伊莉丝所处状况的说明开始……伊莉丝你刚刚问道，现在什麼时候，为什麼这样问？】

【睡眠的地方还是那个地方。在我正要睡着时就想过了，所以醒来後，马上就认识到了这是長期睡眠装置。而且，根據体内魔力的减少来思考，我应该睡了相当长的时间。我想就算是过了数年、数十年的时间也不奇怪。】

看来，伊莉丝的认识似乎是有着如此程度的感觉的。
然而，现实却是不同。
逝去的时间，要更加的长。
然後，不一致的时间把鲁鲁和伊莉丝所知的一切冲刷掉了。
鲁鲁必须把这件事向伊莉丝传达，他抱着半决死的觉悟，说了出来。

【伊莉丝。好好听我说。现在这个时代，比起我们所生长的那个时候，恐怕流过了数千年的日月。】
【诶？】

　伊莉丝，大大的歪着脑袋（疑问）

