







「前方有三个什麼东西。气也是大到了一定程度呢」

「那麼、我去稍微看一下」

「恩、拜托你啦」

我来探测敌人的存在、然後缘野通过使用『穿透』来接近对方观察。
然後、基本上都是由我来奇袭来倒、遗漏的则由权藏处理。
虽然是临时拼凑的队伍但是最近渐渐变得相当协调起来。
不过是岛的西侧、在这呆了三天就已经快要消除隔阂了的样子——

「啊——、有三只奥加（食人魔ogre）。看起来相当的强呢。权藏之类的估计会被轻鬆做掉的感觉」

「闭嘴。你丫还不是攻击力全无啊」

「哎呀、还会用全无这种词汇呢、好厲害哦」

气氛使然啦。
看着两人与其说是彼此不和、不如说是在彼此嬉闹的状态、我不禁有了感慨。
权藏警戒心很弱、又因为直爽的性格、所以很容易和对方打成一片。
一般对待我这种疑心很重精打细算的人的话都会警戒着吧。而权藏这个人的存在、就成了联繫我和缘野的纽带了。
怎麼说呢、权藏这就是主人公体质吧。正统派的攻击方式、稍微有点色......稍微？啊、与那种无关、总之没有讨厌的感觉就是了。

「好好、两人也适合而止吧。说起奥加就是看起来像鬼一样的魔物么」

「是啊。身高2米左右、很魁梧哦。恩、不过男人还是稍微苗条一点比较好呢。头上有两根大角。再就是、牙都从嘴裡突出来了、眼神也是异常的凶恶呢。我倒是比较喜欢柔弱的眼神来着。其他基本与人类无异了」

时而参杂进去的缘野的喜好倒无所谓了、话说是奥加么。确实裡面女性就是食人妖吧。
虽然给人的印象是吃人的拥有坚韧肉体的像鬼一样的魔物、不过就外观的想象应该是没错。

「怎麼办、土屋先生」

「周边没有敌人的样子。能打倒的话就打倒之、因为比较想把它放进搜索列表里」

列表里魔物种类越多、就越容易避开危险的情况、而且偷袭也会变得容易。如果只有一只的话肯定毫不犹豫的就上了但是现在是三只啊。
奥加全身散发着像是燃烧的火焰一般的光、传达着凶猛的气的感觉。强度上的话、虽然只是我个人感觉、但是应该比兽人要强上一个层次。

「好吧、去看看对方情况、如果是无法建立协商的种族的话就打吧。毕竟比起龙和巨人还算是压力小点儿的对手了吧。我靠偷袭基本上会做掉两只吧。不过在那之前——缘野、会说共通语吧。姑且、能拜托你担任一下吸引对方注意力的交涉角色么？」

「真没办法呢」

「如果演变为战鬥的情况、权藏至少要对付一只、没问题么？」

「妥妥的」

确认到两人掌握了情况之後、我们开始移动。
使用『隐藏』消去了身影、我们在奥加前进方向的大树树枝上等着。
虽然这裡也是森林、但是因为树木不是很密集、如果起手失败了的话、我所在的位置也很有可能会被发现。
虽然一口气将线缠到对方脖子上然後吊起来是我得意手段、但是貌似西面的魔物察觉危险的能力都很强的样子、这个方法已经失败多次了。
总之让缘野当诱饵、试试看吧。过去因为让萨乌瓦和格鲁霍担任这样的位置于心不忍、结果担心过度一直没能实行过这个手段。
但是、缘野有『穿透』这个稳定的技能、而且就算發生什麼了也不会太过心痛、所以就无情的使用其当诱饵了。

就在想着这些事的时候气息增强了、奥加拨开杂草、出现在了眼前。
确实就外观来看就是很高的有凶恶眼神的肌肉男。如果没有角的话。
服装是简单的皮革制裤子和七分袖上衣么。既然是手制的话智商等级应该不会低吧。

如果对方是友好的话就好了——最近渐渐很少有这种想法了。

与对方遭遇、如果对方先出手了的话我这边也会迎击。虽然这种战鬥没有後顾之憂、但是由於经常是我先发现的敌人、所以幾乎没有这种循序渐进的情况。
结果、就算对方是友好的态度、也会觉得那只是表象、隔阂还没接触就进入到了谈判阶段、渐渐的精神就极度紧张起来。这就是至今以来的发展。
既然这样、还不如贯彻不说废话只要不是同伴的就全杀掉的方针、这样还轻鬆点吧。

不行不行、最近实在有点好战过头了。處於这个弱者无地位的岛生态之中、我也不知不觉得被传染上那毛病了吧。
如果真想在这个岛上生存下来的话、不能处处树敌、和强力的伙伴进行合作也是必要的。无论变得多强、人少的话能做到的事情还是有限的。
如果这些奥加们是个巨大的组织、因为这次这件事而敌对起来了的话、继兽人之後又要被奥加给盯上了。
杀光所有的魔物和转移者、成为岛上最强就好了。这种极端的想法也曾出现过在我的脑海裡、但是这已经不是无谋的级别而是自杀志愿者的想法了。
得到不输给任何人的力量、轻鬆愉快的除掉眼前的障碍。我并没有抱有这种天真的邪念。世上之事并不会那麼凑巧美好的。

「初次见面」

缘野无声的出现在了奥加的前进方向、打了招呼。
奥加们架起手中的双刃剑、一动不动的凝视着缘野。

「哎呀、语言不通么」

看着浮现出从容的笑容、并没有摆出架势的缘野的态度、对方也有些困惑了。虽然看着是柔软的女人、但是毫无防备的站在那裡、一般都会觉得可疑吧。

「你、人类么」

虽然说得结结巴巴、但是会说共通语的样子。
对方是是可以进行对话的类型么。智商较高如果变成敌人的话很麻烦、如果借此对话的机会、避免敌对的话岛上的威胁也减少了一个啊。

「恩、是人类哦。我们只是有事要去森林的东方、没有战鬥的意思」

一边说着缘野举起双手过肩、表示着自己并没有拿武器这件事。
看到这个场景後、奥加们靠近在一起、开始了协商。

「明白。不打」

老实说还真让人吃惊啊。因为从奥加嘴裡说出了预想外的话呢。
该不会、是要放过我们吧……感觉要成为麻烦事。

「你们、转移者、么？」

「是、是这样哦」

缘野也没有想到会被就此放过吧、显得有些动摇。

「那麼、来村子吧。欢迎」

眼神凶恶的奥加、就这样弯起嘴角笑了出来。

「咦？啊、为什麼」

一个没忍住、我就问了出来。
不单我是这样、缘野大脑也当机了一时没能理解僵在那裡。

『等、等等这是什麼意思啊！』

顺着我缠在缘野脚上的线、她的心声传了过来。

『实在是意外、不过这裡还是接受邀请吧。缘野就这样拜托它们带到村子裡吧』

『别、别开玩笑了！要去你自己去啊！』

这是完全慌了神啊。怎麼说呢、被三名有着那般巨大身躯严肃的奥加护送的话、一般也会感到恐怖吧。而且、她貌似还有男性恐惧症。

『那你就跟他们说、你的同伴也在附近、能一起带去么』

『懂、懂了』

在奥加看不见的角度、用手摆出了个OK的形状。

「我还有同伴、能一起带到村子么？」

「喔、欢迎」

轻易地同意了呢。那麼、这裡就接受邀请吧——由权藏来接受。

『权藏有在听吧。现在到缘野那裡去、和她一起去奥加的村子』

我用和缘野一样连到权藏身上的线传达了心声。

『哈！？我去？土屋先生不去啊』

『对啊、你不準备来了！？』

『我为了以防万一呆在附近。在危急时刻必须要有能出手的角色吧』

好像勉勉强强的理解了我说的话的样子、权藏和缘野合流了。
看到权藏后奥加也毫不动摇的用同样态度说着「来了啊、欢迎」。
然後两个人就这麼在奥加的催促下、跟在了它们後面。
三只奥加露出了破绽百出的後背。现在的话权藏一击即可夺取它们的性命吧、但是还是老实点儿吧。
不过、为什麼奥加对人类没有警戒心呢。
我由於怀疑是否是真话而想将线缠上好发动『精神感应』来读心、但是那不是友好的氛围下该做的。

『总之能先问一下、为什麼对人类不包以警戒心么』

我一边和他们保持着距离的尾行着、一边向权藏下达了指示。

『喔、交给我吧』

权藏毫不犹豫的就这麼用原话、向对方提出了疑问。
奥加一边走着、一边用大到连距离很远的我都能听见的声音说到。

「过去、村子。受到转移者、帮助」

村子、受到了帮助......么。还有着我们之前转移来的转移者对这个奥加的村子施以援手了的事情啊。
如果那是事实的话、那就是托了那位的福才能建立友好关係啊。真是令人感激的事情。
对魔物施以援手的转移者么。和魔物共存共荣......某种意义上、真是让人憧憬的发展呢。






那之後通过权藏又问了几个问题、奥加都用不很流利的共通语、诚实的回答了提问。
顺带一提、他们说的话我是通过权藏的耳朵听到的。这不是『精神感应』的力量而是『同调』的力量。
在前幾天、我消耗了技能点数、把还差一点升级的『同调』升到了等级6。然後就在最近总算是注意到了、貌似等级达到了6之後大多都会觉醒出新的能力的样子。
而对於『同调』来说、不仅仅能使对方与自己同一步调、而且还能反过来能让自己与对方同一步调。也就是说、像现在这样发动了听觉的同调即使离得很远、也能听到同样的声音。
怎麼说呢、因为不触碰对方就不能发动、所以不像这样用线连着的话效果也不怎麼样就是了。

就不多想这些多余的了、将精神集中在对话上吧。
貌似是距今数十年前有转移者来到这个岛上了、那时、由於遭到了其他转移者集团的袭击导致奥加的村子陷入濒临毁灭的状态。
然後拯救了它们的也是转移者。和逃出村子的奥加小孩进行了沟通的一名转移者、打倒了袭击奥加村子的转移者、然後从不可思议的袋子裡掏出了无论怎样的伤都能治癒的液体、拯救了很多受伤的人。
那之後奥加们就变得很感激转移者、如果再次遇见遭到同样境遇的转移者的话一定要热情招待成了村子的规定的样子。

「但是、袭击了村子的也是转移者吧。那样的话、比起感谢、不更应该先抱怨么」

权藏提问出了我也在疑惑的地方。
奥加的男性听到了这个问题後、轻轻点了下头、用仿佛变了一个人一样的眼神看着权藏。

「强大的傢伙、杀掉弱小的傢伙、理所当然。而且、奥加也、有好、有坏。救了、奥加。报恩、而已」

虽然是理所当然的理论、但是听到了这些话的缘野的心声、通过线传达到了我这裡。

『那种事……我都懂啊……』

好像是联想到了自己所做的事、有所感触了吧。现在也是要哭了的样子、孱弱的内心就这麼传达到了我这裡。
如果奥加所说的都属实的话、比起寻找转移者的同伴、和他们合作更让人放心啊。我打从心底这麼想。

「那就是、村子了」

望向處於前头的奥加的手所指的方向、能看到巨大的墙。
在那裡的、不是我们据点或者哥布林村子裡的那种并列圆木组成的围墙、而是岩石筑造出来的十分洋气的城墙在延展着。

「这道墙。转移者、帮忙的」

自夸的奥加洋洋得意的挺着胸。
帮助他们的转移者应该是有着『土操作』一样的技能吧。也可能是有土属性的魔法使吧......下次就拜托格鲁霍、试试看能不能也做出这样的吧。
沿着围墙走着的奥加前方能看到大门。
门好象是铁质的双开门的样子。而在大门前面站着两名穿着皮革铠甲的守卫。

其中一位看起来和这三名奥加一样的、另一位大概就是女性奥加吧。
那长长的白髮、还用胸部那毫不掩饰的巨大的隆起就是证据了。
胸口部分多少敞开着所以能清楚的看到谷间、视觉也被同调的权藏的视线已经钉在那裡了。
真是年轻啊、权藏。这种时候啊、就应该装作看着对方下巴的样子、这样就不会被发现视线聚集到了胸部上哦。

「————！」

守卫的一人、看到了缘野和权藏的身影、惊讶的喊着什麼、但是应该是奥加的语言吧、完全理解不了。
女性的奥加虽然也是同样惊讶的样子、但是总觉得稍微露出了高兴的表情出来。
而且、这个女性奥加。总觉得更接近人类。
虽然身高轻鬆超过了180、但是也就眼角稍微上吊的感觉、而且头上角的大小也就稍微从头髮裡露出一点的程度。
大概、如果戴上帽子的话就幾乎与人类无异了吧。

「喔喔、好久不见的人类了啊。欢迎来到奥加村、转移者的各位！」

　我清清楚楚的听到了那声音不是共通语——而是日语。

