早上一起来，就把昨天的鹿肉装进马车裡继续前行。
鹿的肉成为食物补给，而皮则全部变成了御寒用具。
精灵村是寒带气候，所以皮毛一向很珍贵。
经过了四个小时的奔跑，我们到达了距离补给站10KM的地方，而马已经是极限了，我从马车上下来，让露西艾将马车隐藏在附近的树林。
「西里尔，绝对不要死」
「当然，会带土特产回去的」
必须把过冬的食品带回村裡啊。
「稍微，来这边」
露西艾害羞的向我招手，所以我走到马车上的露西艾的加下。
於是，露西艾弯下腰，将嘴唇贴在我的脸颊上。
「这是为了让你平安回来而设下的魔咒」
简短的说完後，露西艾移开了视线。
因为精灵的耳朵比起人类的耳朵稍微长一点，而且皮肤也很白，所以非常易懂，露西艾的耳朵正在扑哧的变得通红。
相当的害羞吧。
「如果回来，请亲吻嘴唇呦，我会为此而努力的！」
看着害羞的露西艾，稍微的使了下坏。
然後，我为了完成自己的工作，向与露西艾相反的方向迈出了脚步。
「等等！西里尔，我，还没回答！」
从背後传来的露西艾惊慌失措的声音。
我停住脚步，望向露西艾。
「会用舌头所以要有所觉悟哦！」
受到我的追击的露西艾，脸颊变得通红，眼神也开始游离，变的混乱了。
看到这样的露西艾，我大声的笑了。
然後再次开始向前前进。
「西里尔，那个，那个，如果，呜呜……好好的回来的话」
这一次，我没有回头，而是向着背後挥手。
现在回头的话，心裡会产生动摇的。
过了一会，已经不能感觉到背後露西艾的气息了。
「不能死啊」
如果没有享受露西艾的嘴唇就死了的话，会後悔十辈子的吧。
「因此，要认真的去做呢」
体内魔力十分充足。
营养也得到了补充，在马车上也得到了12分的休息。
更重要是，与露西艾的约定给予我的力量。
用强化的肌肉向前迈出步伐的同时，将风聚集到身上，一次向前迈出了5m。
以这个要领，第二步与第三步这样重複的前进着。
这是用最低限度的魔力消费，时速能达到80km/h的高速行走术。
如果无视魔力效率，还能提高速度。
一边跑，一边检查自己的身体的状态。
「固有魔术能使用呢」
我的固有魔术。
是将过去的我呼唤出来。
要是普通的人类充其量叫出全盛时期的肉体的魔术。
但是，我能从到现在走来的30次的人生中选择。
是龙的我，吸血鬼我，机器的我，魔王的时候的我，选项有无数。
但是，想要唤出的话，必须满足一定的肉体与魔力强度的需求。
根據过去的经验。
比起现在的我的战鬥能力出色的有9人。
但现在，只能唤出其中的3人。
残余的6人，魔力，或肉体的容量不足够不能唤出。
如果勤加锻炼的话所有的都会变成能够唤出。
此次选择3人的中最适合此次的作战的我。
那是第18周目时的我。使用起来最方便，也是使用频率最高的我。
10分钟後，补给站已经进入肉眼可见的範围内了。
「【知觉扩张】」
共享风的マナ和知觉。
基地的大小，是300m×200m。
门由铁制成，周围被石墙覆盖着。
建筑物是木材和砖制成的。
在其中，约有300人。
这个补给基地是关口，是以为了用於保护帝国的本土不受异种族的危害的城寨的规模建造者。
用【知觉扩张】侦查着敌兵的配置和拷问出来的结果差不多一样
关於食品和武器的保管库，就相信那个傢伙的话吧（译：可怜的被刺瞎眼睛的队长？）。
「解放吧，吾魂。在时之彼方被遗弃的轨迹，在此显现（中二力严重不足····）」
与属性魔法不同，固有魔法使用的魔力100％来源与自己，所以消费的体内魔力比属性魔法打的多。
「我期望，在虚荣的世界裡高洁的骑士，他乃」
「Dito（ディート，主角前世的名字吧，不知道该咋弄）···【轮回回归】！」
身体被光包围。
（这裡有一句话一直被度娘说是广告·····也不影响，我就不发了····）
慢慢的光逐渐平息，我的身体被铠甲覆盖，变成了装备着双手剑的骑士的姿态
固有魔法与通用魔法不同，不明原理的的再现性的魔法。
「真是令人怀念的身体。」
作为精灵的特徵的耳朵变得圆滑，蓝眼与金髮也被染成黑色。
那个身姿总觉得与帝国的士兵很相似。
「等级14的初学者用的装备……现在的魔力能够再现到这种程度，效果持续时间是48分钟。足够充分了」
我成为等级14时候的Dito的身姿。
在遊戏世界，等级最高为99，不过现在的魔力不足以让我呼唤出99级的Dito。
如果以现在的魔力强行呼唤的话，会在一瞬间因为魔力枯竭而死。
因此叫出了合适的自己。
在这一点上召唤Dito很方便。
因为其他的【轮回回归】，不太存在弱的时候。
「好，出發了！」
浮起会心大胆地笑容，我开始以时速60km向补给站奔跑。
Dito在遊戏时代有着优秀的身体素质，攻击力，和防御力。14级的时候，就有着超越常人5倍的身体素质。
但在魔法的控制这一点却让人失望，这，也是这个身体的缺点
我用跑的姿势直接踢破铁门侵入了补给站。
那个铁门，是要二十人才能打开的沉重的大门，但被我以强化的身体和助跑的冲力给冲开了。
「啊啊，有侵入者！门竟然！？」
在瞭望台上的士兵在發出警报的同时，因为吃惊而变得身体僵直。
在这个时候我已经开始在基地中疾驶。
数秒後，那个士兵，总算意识到自己的工作，弄响了警报钟。
「门被踢破了！是侵入者」
「多少人？」
「只一人」
「一人，怎麼可能打开门！要正确的报告」
被强化的身体清楚的听到了士兵们的声音。
想过配合使用【知觉扩张】,不过因为是Dito的身体的，与风的相性不是很好，原本魔法的控制变得困难，所以放弃了这个方案。
「没有时间。请让我去最短的路径！」
我那样呼喊着，一边面向食品被保管的建筑物一直跑。
根本没有考虑敌人的密度和危险性，只是，简单的向前冲。
「干···」偶然挡在通路的士兵，吃惊的对我的速度發出哀鸣
我架起剑，用剑的质量和成人男性的五倍的肌肉力量轻易的将那名士兵斩断。
如果是普通的剑的话，这麼做会使剑折断。
可是，在遊戏中没有设置损坏功能的我的爱剑忠实地再现了那个功，岂只是没有伤，连血都没有沾上。
从士兵的尸体上散發出蓝色光粒，被我的身体吸入。
这是我爱用Dito的理由之一，他能从被打败的敌人身上获取经验值从而提升能力
我的固有魔术，即使这样的能力也能够成功再现。
魔力是魂的力量。
通常是天生就决定的，即使後期努力训练，也只能有局限性的稍微提高一点而已但是，Dito的【噬魂】由於吸收了对方的魂来强化自己的魂，所以能使魔力增加。
不过用这个方法得到的魔力，与知识和记忆不同，不能承继到以後的我的身上。
「二个，三个，四个！」我依次将妨碍我的士兵杀死
这场战鬥只是我单方面的虐杀。
这边的活动太快，使得士兵们根本攻击不到我。
但是我却能简单的将士兵一击毙命
并且，会变得越来越强。
现在这样无视危险的冒险行为的理由有两个：第一：做出这样的事，会让帝国顾不上去精灵村的士兵没有返回的事情，比起补给站的毁灭，幾个士兵没有返回只是鸡毛蒜皮的小事。帝国肯定会以对袭击者的对策为優先。
第二个，我很生气。
想消除到现在成为牺牲的精灵的朋友们的遗憾。
特别是养育了我和露西艾的老奶奶和露西艾的妹妹。
只是为了得到魔石就挖出心臟这样的事情怎麼可能会允许。
所以我要用【噬魂】饵杀这些东西
「这是什麼怪物！」
「是这样不可能能取胜」
「远离它，包围这个怪物，并用弓箭攻击。这样，即使是这样的怪物也不可能闪的过我们的攻击了吧」
“答案正确”我的内心答道
确实来自全方位的包围攻击是不可能避开的。
直到刚才为止一直在恐惧的士兵，都拉起弓向我射来。
我尽量躲闪能够闪掉的，躲避掉的用剑砍掉,不过，尽管如此仍有幾支箭射到。还有一支箭运气不好的射到了眼睛上。
眼睛是人类绝对无法锻炼的地方。
「太好了！」
在我正面的一个士兵喊到。
我浮起笑容猛扑过去。
「没用哦」
然後，将他劈成两半，并夺走了魂。
「现在的我的身体，相当硬呢」
「射到眼睛的箭竟然被弹开了」
「那样的生物不可能存在」
「是···是恶魔，那个傢伙是恶魔！」
我的身体的防御力被反映为硬度，而且是钢的铠甲的防御力。
然後，Dito根據自己的特殊能力不存在要害。
一般人射中心臟，头颅的话一般会当场死亡。
但是，Dito所受到的攻击，只会变为伤害这个概念，甚至连伤口也不会留下一个。
只是HP这个虚构的数值的减少。
如果HP归0，就会死亡,不过直到死亡的时候，也不会出现身体钝化
没有疼痛，也不会疲劳，就如同不死之身一样
对方较弱的情况下，Dito是无敌的。
强敌对方光是用身体素质的强化不对抗断，HP一瞬成为0。
我将纠缠着我的30名左右的士兵斩杀，终於来到了食品保管仓库。
并用剑打坏墙，进入其中。
「厲害」
我不禁發出声音。
食品超出想象的多。
粗略的看，换算成小麦的话大约有4吨。
而且有幾样很感兴趣东西。
例如：马铃薯和豆子这样精灵村没培育的谷物和蜂蜜酒，甚至还有盐。
这个是帝国的支配形式造成的吧。
作为从其他幾个村榨取资源的中继站，这裡集中了大量资源
「【物品箱】」
把那些食品，都放入物品箱中。
这个也是Dito以外的人不能使用的技能，也是今天选出Dito的最大的理由。
遊戏时代的重量限制是4000，现实中1用1kg换算能自由地收纳4t物品。
放在【物品箱】的东西，无论什麼时候都能取出，放而且也不会腐烂，唯一的限制是不能放入活的东西。
「那麼，吃的东西弄好了。之後是武器」
一边受到从背後而来的士兵们的弓箭的攻击，一边踢破门到旁边的武器库。
士兵从帝国出来时为了食品和武器都是最低限度，所以这个补给站的武器也十分充足。
使用【轮回回归】已经30分钟了。
还有18分钟就会变回原来的身体。
如果变成那样就只有等死的分了。
以这个轮回的西里尔的身体根本不可能从这裡逃出去。
为了抓紧时间，我一边砍飞挡路的敌人，一边冲进武器库
「全是铁制的武器这麼！不愧是帝国啊」
连普通的士兵所装备的，虽说是低劣品，但也是铁质的。在这个武器库裡，保存着50套铠甲和铁件。
即使一套30kg左右也能拿走30个。
「那麼，先清一下行李吧」我把物品箱打开，取出袭击村的士兵们的尸体，混在来的路上杀死的士兵裡。
虽然希望不大，但希望帝国能以为他们是死于这次袭击。
可惜的是，经过残酷拷问的队长的尸体没有带到
「那麼带着战利品先回去吧」今天，一共获得了食品3t和铁1t。
如果有近3t的食品，足够200个村民一个月的生活所需，再加上村子的储备和平时打猎的收获的话，足够让村子熬过冬天。

1t铁，除了保留部分武器和铠甲以外，剩下的用来加固村裡的木质农具，和制作带刺铁丝，这样就可以更好的利用森林的资源
「要憋足气啊」我一边那样说着，一边踢破墙飞出，一边受到弓的集火，一边将数十人砍杀并飞奔出门。
从後面听到了追赶而来的马蹄声，不过那都是徒劳，现在的我，只要用力跑，就能比马还快。
Dito能够持续的时间还有4分钟，比想象的要耗时。
大脑中浮现起露西艾的笑容，想到还没有享受过那个嘴唇怎麼可能死，身体稍微恢復了点元气。
突然身体光被包围。
黑髮·黑眼再一次变成金髮·碧眼，铠甲也消失。
我从Dito，变会了西里尔。
与此同时强烈的倦怠感袭击全身，这是魔力枯竭的现象
而且，【轮回回归】的副作用也随之而来。
魂吱吱嘎嘎地响着，發出了悲鸣声
「能在到达界限之前甩开追兵真是幸运啊」【轮回回归】，消耗了全部的魔力，想要在此使用的话，起码要12个小时之後。
【轮回回归】是用魔法欺骗世界，把如今的西里尔变成过去的我，但，由於歪曲了事实，所以灵魂和肉体并不相配，所以都会受到超负荷的损伤，这个魔法是会损伤灵魂的双刃剑，如果连续使用，不单单会死，连灵魂也会破没，不能在此轮回转世。
「【知觉扩张】」发动精灵的风魔法，自己现在的魔力幾乎不能使用，但因为和风的マナ的相性，能够勉强发动
锁定露西艾的身姿，向那裡迈出沉重的脚步。
深夜二点，总算是到达了，在所有人都应该在睡觉的时间，露西艾依旧在马车外面披着毛毯等着我。
而且，为了不被敌人发现，所以露西艾没有生火，应该会相当冷的吧。
「西里尔！太棒了，平安的回来了」露西艾跑到我身旁将我抱住，说道。
由於森林的夜晚很冷，露西艾的身体也并不暖和
但是，却觉得那比什麼都温暖。
「并没有受伤哦，担心了麼？」
「嗯，非常，非常地担心」露西艾把脸埋在我的胸口。
那裡有着冰冷的水的触觉。
大概露西艾哭着。
「我可不会简简单单的就死掉哦，还有食品成功的抢到手了」
。
「只要西里尔平安就好」
「我一点事也没有。而且还抢到了食物。还没有和露西艾接吻，怎麼能死掉」
「那个，是认真的吗？」
「嗯，认真的。当然，如果露西艾讨厌的话，是不会做的」
想和露西艾接吻,不过如果因此被讨厌的话就什麼都没了。
我想与露西艾通过心相互连接着。
「好哟。是西里尔的话」
露西艾举起对埋在我胸口的脸。
眼眼睛裡含着泪珠，脸颊染成了粉红色。
「谢谢，露西艾」
虽然只是嘴唇的接吻，并没有性的快感,不过，心裡变得暖和起来。
生活的真实感不断涌出。
曾经断裂的感情也慢慢復甦
下次在放入舌头，这次，仅仅是这样就满足了
「西里尔，这是我的初吻呦。要好好的负责啊」
「荣幸之至，公主大人」
说出这句话的同时，我身体剩餘的力量彻底耗尽
「西里尔，没关係么！？」露西艾担心的靠了过来
「不是伤病了了。，只是魔力耗尽了，还有刚才使用的魔法的副作用」
「那个，真的不要紧么！」
「好好睡一觉就能好了。反过来说，不睡一觉的话永远不会回復，把我搬到马车的装货台裡面吧，为了避免感冒要披上毛毯」
「在我起来之前在这裡不要动，如果，敌人追来的话就放弃我和马车，自己一个人逃跑，精灵是不可能在森林中被抓住的吧」说完之後，我的意识彻底消散，进入了睡梦之中。
