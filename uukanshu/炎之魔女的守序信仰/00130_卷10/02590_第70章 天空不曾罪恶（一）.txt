ps：想听到更多你们的声音，想收到更多你们的建议，现在就搜索微信公众号“qdread”并加关注，给《炎之魔女的守序信仰》更多支持！

相比於骚动的地面，上面的世界则要安静许多。

四位主神在天界的大殿内，纷纷坐在自己的宝座上。

环视一下他们之後……

主神们纷纷将视线集中过来，露出和善的笑容。

只要自己有一点点动作他们也会非常在意，在意的过头了。

那种不自然可能会让刚来到这裡的傢伙毛骨悚然。

但是他百无聊赖地偏过头去。

早就已经习惯了。

一望无际的天空中。

只有他在观望着一切……

即使在这个世界的任何一处，他都能掌控。

西边的大陆稍微和平一些，但是魔物们则是计划着夺取这一切。

南边则是一如既往，妖精和兽人们好像已经有所察觉。

但是东边遍却显得糟糕多了，战火纷飞，尸体堆积成山。

对此，他有些心痛。

每一个个体都是他创造出来的。

从地面苟且偷生的小虫……

直到坐在他的身边，至高无上的诸神和精灵。

除了那两个天使之外……

她们会怎麼样呢？

现在还看不出答案。

破坏神，勇者，魔女，天使。

谁能最终到达我的身边？

不管是谁……

之後的世界就交给他们来管理。虽然不知道能做到何种程度，也许就此就会消亡。

但如果那就是命中注定的话。也没有办法。

神和天使们没有思考的能力。

活物一旦会思考就不可能永远保持不变。

所以，他让凡人思考。却不让神和天使思考。

离开这裡的时候也让他们拥有灵魂好了？

看着一直完美微笑着的诸神。

他也苦笑了。

“早上好，小阿……说起来……明明周围都是机械风格，只有这裡是这样的……你的品位一定坏掉了呢。”

这个时候，宫殿裡有了新的来客。

周围的神通通低下头表示尊敬。

这裡只有一个人有资格和她对话……

但是对於王座上的他来说并不是很想见到的人。

“只是随机创造了……还有我应该已经禁止华纳族进入这裡了，你那副身体是从哪来的？”

看着一头漆黑秀髮，穿着学生装的少女，最顶端王座的他皱起了眉头。

“那种术式稍微花点时间就破了啊，别总是一脸苦瓜相嘛……那张漂亮的脸都浪费了哟。”

“身体对我们没有意义，是你享乐太过了。盖娅那边放置着没关係吗？”

听到王座上的女孩摆出这副说教般的面孔，学生装少女摇了摇头。

阿尔克纳拉，世界的创造神。

他的模样这样看起来也是一位美丽的少女，淡金色的长髮在脑後束起之後仍然长的能拖到地面。

胸部的分量看起来很大，尤其是这种只有皮草和布料类似旗袍的衣服更显得她身材姣好了。

这倒并不是出於个人兴趣什麼的……

“小阿出生时的性别是女孩吧？”

“早就忘记了……都说了这样的事情一点也不重要，你难道记得自己用过多少个‘替身’吗？”

“啊，骗人！明明听到了我为你送了个男生过来的时候非常期待的！”

她不太想提及这个话题。

与偏向欲-望的华纳族，阿萨族的他倒是偏向於秩序和传统。

“你还没回答我的问题呢，盖娅那边没问题吗？”

为了撇开话题。阿尔克纳拉做出有点生气的样子，他吊着大眼睛，从宝座上站起了身。

白皙细嫩的大腿从开叉的裙摆裡露出一大半，那裡看不到有穿下着。

“喔~~眼福呢。”

学生装的少女将手指交互构成方框作出拍摄的动作。

“你真是个奇怪的傢伙。即使在一群奇葩玩意的华纳族裡也是。”

“也许是扮演人类太久不能为所欲为，积攒的太多的缘故~~”

“这样迟早会忘记自己的身份……再也回不到神的世界。”

类似的例子已经见过太多了，作为一个上级神族。阿尔克纳拉比其他神拥有更多的权力。

许多同族太过於融入自己管理的世界，最终忘记了身份和使命。精神消逝在凡人的身体裡……

对於没有消亡概念的神族来说，这就是他们的终结。

“你在担心我吗？小阿~~好开心。”

“别恶心人啊……和我相反你出生的性别是男性吧！”

“终於承认了呢。不过我记得很清楚哟，而且在盖娅也扮演男性呢……”

“呼呼，真是个怪咖。”

在她的这番折腾下阿尔克纳拉终於微微一笑。

“哈哈，你终於笑了~~”

“才……才没有。”

创造神的脸稍微红了一下，然後立刻又回到那副严肃的样子。

即使两位神族这样闲聊着，守在一旁的神和天使们也没有半点神态的变化。

如同装饰在室内的人偶。

他们就静静地在这裡对视了一会。

“真的要放弃吗？”

“嗯，已经累了……”

“是吗……虽然也不是什麼好事，这个世界很不错啊，这样独断一定会被上面的神责骂吧，不过也没什麼大不了的，我支持你的想法哟。”

“谢谢。”

创造神小声地说了，当初是自己要来管理这个世界的，但是现在却要逃避这份责任。

原因只有一个……

真的不想再继续下去了。

少女的话对现在的他来说无疑是最大的鼓励。

但是可能是长期孤独地呆在天界，他还是太不擅于言辞……不知道怎麼表示这种谢意。

不过赶在阿尔克纳拉再次想开口之前，少女仿佛想到什麼一般先说话了。

“对了，那个孩子还请不要伤害她。”

“放心吧……我没有那个意思。”

“嗯，她毕竟曾经是我的友人呢~~”

“友人……吗……”

这个词让他再次陷入消沉。

学生妹慌忙摆手安慰起他来，再怎麼说对方也是个女孩。

已经习惯做人类的她很害怕女孩子陷入消沉，

“别，别这样啦！我也是小阿的友人啊。”

“也是呢……”

“之後离开这个世界後要来盖娅吗？”

“我会考虑的。”

“嗯，我等你哟，掰掰~~”

说完，少女就突然消失在大厅裡。

阿尔克纳拉脱力般地坐在王座上……

“好累……但是，快结束了呢。”

地平线上的旭日缓缓升起。

世界的末日开始了最终的倒计时。(小说《炎之魔女的守序信仰》将在官方微信平台上有更多新鲜内容哦，同时还有100%抽奖大礼送给大家！现在就开启微信，点击右上方“+”号“添加朋友”，搜索公众号“qdread”并关注，速度抓紧啦！)(未完待续。。)

...

...  	() 