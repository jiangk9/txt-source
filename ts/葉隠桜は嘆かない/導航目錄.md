# CONTENTS

葉隠桜は嘆かない  
葉隱櫻不悲嘆  

作者： 玖洞  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%91%89%E9%9A%A0%E6%A1%9C%E3%81%AF%E5%98%86%E3%81%8B%E3%81%AA%E3%81%84.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/ts/%E8%91%89%E9%9A%B1%E6%AB%BB%E4%B8%8D%E6%82%B2%E5%98%86.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/ts/out/%E8%91%89%E9%9A%B1%E6%AB%BB%E4%B8%8D%E6%82%B2%E5%98%86.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/ts/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/ts/葉隠桜は嘆かない/導航目錄.md "導航目錄")




## [一章](00000_%E4%B8%80%E7%AB%A0)

- [001　序章](00000_%E4%B8%80%E7%AB%A0/00010_001%E3%80%80%E5%BA%8F%E7%AB%A0.txt)

