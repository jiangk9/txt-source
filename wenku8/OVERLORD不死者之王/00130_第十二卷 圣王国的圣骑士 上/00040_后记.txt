
小时候，一边挨爸妈骂一边写剩下的暑假作业时，或是进入八月翻开日历时，应该有很多人幻想八月能延长到六十日吧？

丸山也每年都这麼想，而且是在九月的第一天，在学校一边举手说忘了写作业一边妄想。

於是这次，我尝试将这种幻想化作现实！丸山曾经希望能成为实现孩子梦想的大人，如今希望成真了！这实在是太美妙了！

各位的──差不多该适可而止了。讲些没用的藉口拖延时间也不是办法嘛。

事情就是这样，虽然比预定日期晚了一点，但总算是出版了。哎，我想应该还在误差的範围内吧。不是，真的發生了很多事啦，好事坏事都一大堆。

不过话说回来，丸山在住院期间，也买了几本电子书来看，但电子书还真不错呢！没想到会那麼方便，真的让我觉得《OVERLORD》或许也可以推出电子书版本。因此，再过不久《OVERLORD》也要电子化了。人啊，很多事情还是要自己试过才知道呢。同样的，也有很多状况要亲身经歷才能明白。

顺便提个题外话，我看的电子书是漫画，幾乎都是恋爱喜剧类。

那麼後记到了尾声，这次也请让我感谢众多人士，特别是赏光买下本书的各位读者。还有某家医院。

好，那麼希望下一集还能与各位相见。谢谢大家。

二○一七年九月　丸山くがね
