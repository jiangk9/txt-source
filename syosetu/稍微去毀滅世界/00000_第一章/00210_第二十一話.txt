
心什麼的已经被抑制的盗贼公会会长和幹部们，在那个时候对抗累斗的手段已经没有了。
无论说什麼就只能点头的他们，张罗着继承有关盗贼公会的权限，只不过是比起轻而易举更为简单的作业。
可是，虽然在这裡让他们点头也没有什麼强制力，可是如果去掉累斗的恩惠之力的影响的話，就又会变回原来的状态了。
就算是累斗也不会只是为了入手盗贼公会就一直持续给予蒙贝和幹部们恩惠的影响。
在突入盗贼公会之前，打算在商议後带着公会长等人去马修的店裡，在那裡，设法準备在人族和兽人族的奴隷身上所使用的有隶属效果的项圈。不过累斗在之前的行动中收拾掉的在天花板裡和地板下隐藏的暗杀者集团，在处理掉他们之後入手的技能中，累斗注意到了其中一个。
那是叫做＜契約＞的技能，用单眼眼镜的鉴定功能调查了一下，看来好像是结成隶属纹和隶属项圈、或者通常有强制力的契约等的技能。
当然，入手之後并没有像其它的技能那样升级了，是否实用，累斗感到不安。是对等的契约还是只能跟低等级的对手契约呢？累斗在持续使用恩惠之力的期间，对手是不能拒绝的，所以无论多麼乱来的契约都可能缔结。＜契約＞技能的高低完全没有关係。
是关於用契约书维持强制力的魔法，这是技能附带的知识吧，累斗总觉得能理解。让在恩惠影响下的幹部的一人準备了纸和笔，累斗马上做完了人数份的契约书。

「首先，让给我人事权、决策权和命令权。表面上是由公会长蒙贝来做，最终决定由我来下达，这样可以吗？」

「好的。」

「蒙贝以下的幹部，削减报酬的三成，请将那部分支付给我。其他的一如既往就可以了。关於公会的财产也请让我自由的使用。」

「是。」

「对在公会内部的亚人，禁止虐待轻蔑什麼的。还有最優先保护从这个街市带来的亚人。那时候如果流入市场的話就购入。如果是冒险者带来的状态的話，进行协商，不行的話让冒险者消失，保护亚人。」

「是。」

「当然，禁止一切危害我和我的关係者的行为。」

「是。」

「有意图的违反契约内容的行为，直到得到我的允许，否则将会持续受到快要死却死不了的剧痛的惩罚，没问题的吧？」

「是。」

「其他的，如果想起什麼了会进行追加的，追加条件要无条件服从。如果没问题了的話能请大家将名字写在这张纸上的栏内吗？」

通常的話，如此多的无理要求，是相当异常的事态不可能会被通过的。幹部们和蒙贝老老实实的继续点头，最後累斗将那些事项记下来，然後给他们看使用了＜契約＞技能的文件，他们没有抵抗的、默默拿出笔在文件上签上了自己的名字，有的人用了印章，还有的人用自己的指印进行画押。
让全员做完后，累斗首先检查了文件，确认了没有不妥後满足的点了点头，然後将文件收纳到收纳指环中。

「就这样，这个盗贼公会已经名正言顺地成为我的了。好的，大家鼓掌。」

劈劈啪啪，屋子裡充满了无力的掌声。
累斗一边听着掌声，一边停止了在场所施加的恩惠的影响。
尽管停止了恩惠，但是暂时蒙贝他们是没有脱离影响吧，持续着无力的鼓掌。不久後开始脱离影响，意识到自己到底在做些什麼後停止了鼓掌，一脸愤怒的站了起来。
但是累斗一脸清爽，若无其事的样子，没有想要从现场逃走的意思。

「竟然做了这样的事……会变成什麼样你是知道吧？」

蒙贝一脸愤怒的大声喊到，不过谁也没有上前去抓累斗，恐怕也没有想要抽出携带的武器吧。
那是因为，此时时刻，都已经是徒劳的了，是谁都知道的事情了。

「那是没办法的吧？还是说还有没在场的打算杀害我的幹部吗？」

完全是偶然，累斗将盗贼公会的上层在这裡一网打尽了。
全员已在＜契約＞技能作用的文件上签了字，如今在盗贼公会的上层应该不存在持有加害累斗权限的人了。
仿佛是在印证那个说法似的，累斗以外的全员因愤怒和悔恨扭曲了脸。

「如果有的話，我命令到，请将其无力化，无论使用什麼样的手段。」

从蒙贝他们的表情看来，应该是没有那样的人。以防万一，累斗基於契约的权限向蒙贝他们下了命令。
蒙贝他们无法拒绝这个命令，累斗不知道的他们知道，如果有想要加害累斗的人的話，他们应该会团结一致的将其无力化。

「那麼，到这儿我的事情就完成了。还会再来的，到时候，为了来这裡没有麻烦请好好安排哟。」

事情办完了留在这裡也没什麼意义。
让马修照看娅莉艾丝她们不算太长的时间，累斗考虑到对马修有些过意不去，并有报告事情的来龙去脉的意图，於是打算回马修那裡。



「啊，对了，天花板裡和地板下的人们，要好好地回收吊慰後安葬。还有麻烦选出在这条街上开店好的地点。餐馆相关，拜托了。」

「竟然说开店……？」

到底想幹什麼呀，全员脑海中浮现出这个疑问。就算对於累斗的寻问没有得到回答，也不能就认为可以无视命令。
如果那麼做了就是违反＜契約＞，根據在累斗拿着的契约书上刻着的魔法之力，蒙贝他们将持续遭受难以忍受的痛苦。
如果不想那样的話，蒙贝他们只能服从累斗的命令。

「即使用尽一切手段袭击了我，而重要的契约书收纳在魔法道具裡，不是我的話是没办法取出来的，这个事先告诉你们。」

「你……」

有谁不明真相，又有谁因为累斗的話而呻吟起来。
恐怕用什麼方法从累斗那裡夺取契约书，将其破坏的話就能逆转，有着这麼想的人。累斗对此没什麼兴趣，离开了那裡。
回去的路上远比来时轻鬆的多。
毕竟来的时候差不多将妨碍都除去了。比什麼都好的是完合控制了公会长和幹部们。
要说公会的上层和累斗之间發生了什麼，不会马上传播开的。尽管如此，对於闯入上层的会议还平安回去了的累斗，考虑到一定發生了什麼的吧，可对於累斗出手的人是没有的。
就这样，用恩惠和技能之力掌握了盗贼公会的累斗，在回去的路上经过中央广场时，注意到了周围的变化。

「这、是？」

从时间上看还没到落日的时间。
累斗在盗贼公会消费了不算多的时间，在自己正準备穿过的中央广场时，突然意识到明明是这个时段，在广场的正中，除了自己以外却没有任何人。於是停下脚步。
当然这是异常的事态。
在街市中心的广场上，累斗以外谁都没有，平常的話是不可能的。
即使没有特别的活动，由於位置的关係这也是自然而然会聚集人群的场所。以在那聚集的人们为目标的摊子也林立在那裡。在那裡，有为了防止纷争而来回走动的卫兵之类的。那裡就是中央广场。
但是，虽说是午後，但太阳还很高，明明是这个时段，却连一个人都没有，不管怎麼样想都是奇怪的事态。
奇怪的事态是指，大概被招呼进麻烦中了。
已经明了的累斗在注意到了异常的时候试着从广场往外走，但是通向广场的道路不知为何有着分界线那样看不见的墙壁，妨碍了累斗的前进。
因某些理由，用了某些方法将自己困在广场，累斗注意到的瞬间，在广场的最中央设置的女神像發光了。
为了防止视野被灼伤，累斗立刻用胳膊遮住眼睛。不过光随即消失，累斗一边眯起眼睛一边将胳膊放下来，在那之前，女神像所在的场所简直就像间歇泉一样，鲜红的粘糊糊什麼东西被吹向空中。
那看来是血的样子，注意到了的累斗，想起了那裡是到现在为止杀了许多的兽人族、为了献给女神露米艾尔的场所。

「难道不会是想让我进行强制遭遇战鬥事件吧。」

没办法逃走，喷射出的血的奔流没有停止的迹象。
再加上累斗现在没有武装，指环只收纳了短剑的样子，如此这般是幸运吗，於是累斗立即从收纳中取出短剑。
从在盗贼公会消灭了的暗杀者们那裡回收的技能中有着＜暗殺術＞，基本上是使用短剑类似的武器战鬥的技术。
同时累斗掌握了相同等级的剑术，虽说知道使用的方法，不过在累斗看来，长剑那样的武器太重了，实在是没有办法。
关於这点，短剑的話就能轻鬆的使用了，这样的話不久前入手了＜暗殺術＞这样的技能真是幸运，累斗如此认为。
右手倒握着取出的短剑，一边向前伸出右手一边沉下腰来，就那样準确地摆好了架势，累斗注意力仍然在喷射不断的血之间歇泉那边。

「汝、吾神之仇敌。」



喷出後落到地面、将周围染红了的血慢慢的开始聚集，形成了一个形状。
实在是令人毛骨悚然的光景，累斗皱起眉头，从眼前凄惨的光景中是无法想象的，回荡着低沉冷静的男人的声音。

「从那身上漂浮着气息，看到了女神罗洁之物。」

血凝固了变成了黑色，随着表面变得光滑并开始放出金属的光泽。
变化成了人的形态，手中握着什麼又长又大的武器，全身覆盖着黑铁的铠甲，从裡面發光的一对眼睛盯着自己，累斗感到背後稍微出了些冷汗。

「排除不归顺女神者。」

「说什麼不归顺女神什麼的，这边是遵从这边女神的指示呢。」

不久血的间歇泉停止了。
喷射出的血变成的是，黑铁铠甲的姿态，大约比累斗高两个头，手裡拿着斧和枪合为一体那样的武器的戟，那样的战士。
出现在了人族的街上，材料恐怕是在这裡砍了多少个兽人而渗到石板裡的血吧，累斗推测突然出现的战士应该是女神露米艾尔的东西吧。
试着考虑的话，累斗在人族的领域报出了罗洁的名号，作为罗洁的使徒没怎麼考虑便一味的到处使用着力量的样子。
对於累斗来说没打算夸张的到处活动，尽管如此也十分足够让女神露米艾尔發现了。

「虽是这样说，但相当强硬的感觉呢。」

累斗听说，本来，中央广场的女神像那裡，是根據奉献的供品为了得到女神的使徒和女神的加护的地方。
明明应该是那样的，可是从那裡出现的不是使徒也不是加护，只是个战士。
累斗考虑到，恐怕这是奉献的血量没有达到规定量，勿忙强行集中力量做出来的吧。
也就是说，在这裡女神露米艾尔的力量是不安要素，虽然感知到了累斗存在，不过至於有效的排除的程度是不高的。
那麼是不是有机可乘呢，累斗这麼想着。
至少，现在在这裡打倒这个黑铁的战士，之後有着马修的助力和盗贼公会的力量将街市入手，之後在这条街上不会奉献兽人族的供品，不是能弱化女神露米艾尔的力量吗。
那麼在这裡稍稍努力一下，排除了这个黑铁战士的话，之後就轻鬆了。
这麼考虑的话稍稍兴奋了起来，於是累斗手握短剑注入力量。与累斗相对黑铁战士两手持戟摆好架势，立刻以累斗为目标砍过去。


-- Fin --
