
那麼，我们现在先得到接待处进行冒险者登记。
在艾伦指示方向的接待窗口，有一个有着漂亮银髮的接待小姐，看起来和茜的年龄差不多，大概15、16岁这样吧？好像是要在那裡置办各种手续。
我过去向少女搭話：“打扰了，我希望注册成为冒险者。”

另外，在接待窗口上的名字版上标记着“舒碧儿”。舒碧儿注意到了我之後，非常惶恐的低下头。

“非常欢迎您的到来，救世主大人，我们冒险者公会一直恭候您的到来，我是公会会长的女兒舒碧儿，今天接到命令前来接待您。”

什麼啊，原来是多兰的女兒，特意为了我而派遣女兒过来的吗？是不是有些关注过头了，不若说，没必要这麼客气。即便如此，竟然叫我救世主吗，虽然没有错，但感觉真难为情。如果单从稳定经营孤兒院这方面来看的話，“救世主”这种等级的称号是多餘的，虽然从结果来看是拯救了国家，但实在是太夸张了。
於是我“拜托了，不要这麼称呼我，我的确是想拯救这个国家，不过我更想悄悄活下去。”这麼说了。但是，舒碧儿小姐却摇摇头。

“虽然我可以这麼做恶，但是，因为公会会长的父亲就是这麼称呼您的，在公开场合我也会称呼您为救世主，但在私下我可以依照您说的做，还请见谅。”

舒碧儿一副为难的样子。“公开场合没办法吗，好吧。。。”“救世主”这样的称呼真的很难为情，真希望一直都不要这麼叫。話虽如此，这也并不是她故意为难我，我也不能指责她，我决定等到了公开场合好好忍耐。
於是，我想他传达了谅解之意。

“谢谢！”她深深地低下头表达自己的谢意。其实不用感谢也行。
而露西娅她们则是……

“救世主啊，感觉真的很适合主人哦。”
“艾琳也希望大家都能这样称呼真嗣大人。”
“茜，茜也是。”

她们开始热烈的讨论，嗯，我想我确实可以拯救这个世界（译：蜜汁自信），但我更想低调的生活。
於是我说：“用那个称呼方式的話或让人觉得很陌生，简直就像不熟悉的人一样呢。”少女开始焦急起来。“啊，还是请让我叫主人吧。”
“艾琳也拜托了。”
“请让茜叫您真嗣先生。”
“嗯，随你们喜欢的称呼我吧。”

露西娅她们开心的微笑了。 把这些事情放在一边，赶紧去登记冒险者吧。

“关於登记手续的办理我需要怎麼做。”
“是这样的，本来的話注册冒险者是需要接受幾个考试，并进行判断的系统。”

舒碧儿的脸上露出了为难的表情。嗯？

“其实……关於真嗣先生您并没有可以接受的考试。”
“啊？没有能接受的考试？这是怎麼回事？”

我發出疑问的話语。

“真的很抱歉！”舒碧儿深深地低下头。

“我并不是要你道歉，比起这个，我更希望知道原因是什麼。”

我的話使得舒碧儿再次惶恐地深深低下头进行说明，虽然没必要低头就是了。

“请让我说明一下。实际上，冒险者公会準备的考试本身，并没有达到能够考验真嗣先生的水平，也就是说，如果真嗣大人去进行考试，反而会暴露出许多考试本身的缺陷。”

啊，原来如此，当我理解这个事太之後不禁苦笑。也就是说因为我的实力太高了，由於考试本身的问题才出现这样的情况。试着考虑一下便觉得理所当然了。恐怕是因为至今为止都没有我这种等级的冒险者吧，所以用来测量实力的考试并没有準备好。

“主人您已经超越标准了呢。”
“居然连考试都无效了呢”
“要测量真嗣先生还是太难了。”

但，如果是这样的話……

“这样的話，不能接受考试的我，是不能成为冒险者的吧。”

我遗憾地说，“不，不是那样的！”舒碧儿慌慌张张的反驳，她的脸上满是焦躁，好像我一走世界就会完蛋似的。

“但是，不考试的話，不能成为冒险者是规则吧？”

对与我的提问，“不，并不会！父亲说了对於真嗣先生的考试全部免除！而且，您是世界上第一个拥有能够获得SSS级冒险者资格的人，请成为冒险者！是有您才是这个国家的……不，是世界的希望！”舒碧儿拼命的恳求，我会是这麼重要的人吗？（译：作者你是故意恶心我吗？）

然後，从远处窥视着我们这边情况的冒险者们，惊讶的声音也随之提高，公会内一阵骚动。

“不，不会吧，居然是SSS！？”
“不知道啊，SSS真的存在吗？”
“对了，SSS确实存在过一次，那大概是在神話时代，神明降临的时候被授予的等级，那可是传说中的等级啊！那个男人居然被授予了这个等级，可恶！难不成他还是神話等级的吗？”

後方传来这样的声音。

“主人实在太厲害了，没想到居然成为了SSS级冒险者。”
“当然的吧，真嗣大人可是世界最强的哦。”
“终於达到世界级别了啊真嗣先生～”

少女们也这麼说，我稍微考虑了一下，便摇摇头，

“不，我还是不能成为冒险者啊。”

於是，舒碧儿立刻脸色发青，“为，为什麼不同意呢，请您成为冒险者，拯救这个世界！救世主大人，拜托您了！”

好像是因为世界末日一般的害怕，嗯，她理解错了，冷静考虑一下就可以明白了。

“我不想搞特殊待遇，所以我要考试，明白了吗？”

看来总算是理解我的意思了，舒碧儿乖乖的点点头。

“真嗣先生明明是最强的，却又明白公平与规则的重要性，果然与我们是不同次元的人啊。”

舒碧儿的眼裡满是敬畏。

“也不是多麼了不起的事儿，越是位於高出，就越应懂得遵守规则的重要性，上面的人遵守规则，下面的人才会愿意一起遵守规则啊。”

於是舒碧儿敬畏的目光越发强烈了。“没想到救世主大人不仅仅是有实力而已，就连内心的想法都如此高尚，我会好好谨记您的教诲并传授於他人的。”舒碧儿带着湿润的目光看向这边，无论我怎麼说也只会让她的敬畏越发强烈……唉，为什麼会这样呢，如今也只好顺其自然了。
不管怎麼样，我已经决定参加冒险者考试了。

