巴别利奇・帕多利克。帕多利克家现任当主，卡丽娅・帕多利克都生父。
听闻，自从帕多利克家落魄到骑士阶级以来，在一向重武的帕多利克家中，巴别利奇是少有的艺术与政治造诣深厚的人物
然而，从他那刻印在右眼上纵向撕裂的战争伤痕以及那几乎能让小儿啼泣不止的面容来看，我心中不禁涌现出无尽的疑问，这情报到底可靠吗。倒不如说，战斗在第一线才是其人生价值所在这种说法反而更让人能容易理解。刺绣在其胸前的科里登堡总督之证，与剑和鹰的装饰相当般配。
巴别利奇・帕多利克，漫不经心的指向我说道。

【卡丽娅，这是你这家伙的从者吗】
【是的，父亲大人――失礼了，阁下。虽不是正式的，但差不多类似】

现在，这家伙莫不是在不经意间作出了无比荒唐的发言。
我瞪圆着眼睛，凝视着石板。我现在很想插嘴进行修正。不管从哪个部分看，我应该都不曾有过成为卡丽娅・帕多利克从者这样的大事件。
然而。现在我别说是发声，搞不好连抬头都不被允许。如果庶民以骑士阶级的当主为对象作出这种事情的話，对方或许会非常欣然的把我的首级砍下吧。在这其中并不涉及诸如人情诸如利害一类的东西。貴族社会，騎士社会，最后甚至是平民。这是一种明确上下级关系的习惯。
巴别利奇深深的叹着气，对我们发声，正确而言，是朝向卡丽娅投出了声音。

【真是捡回来了一个难看的从者啊。要说符合你的风格的話，嘛也是符合的从者】

这是，什么意思呢。对这突然抛出的話语，我头脑中产生了谜团。这是说卡丽娅・帕多利克性格奇特的意思吗。如果是这样的話，确实有不可否定之处。可是，如果把现在所选取的一处地方变换一下的話――于她而言，听起来就像是蔑视的言辞。
那声音，简直不像父亲对女儿该有的声音，非常的低沉，毫无感情。

【请您恕我冒昧相告——您所言，这份传令书能够平安送达，也有他的功劳。他绝非无用之辈】

如此说着，卡丽娅・帕多利克稍微挪动了一下身子，从怀中取出信。虽然已经满是褶皱，不过或许是托了她的福吧，外观已经稍微能看了一点。

　一一总之，这样工作就结束了吧。说老实話，松了一口气。我暗自舒着气，微微抬起视线。朝着帕多利克接取了信的手边看去。

之后，接受形式上的赞赏，或者只是一句辛苦的話后――正如此想，的时候，突然的，巴别利奇的两只大手，把刚刚送达的，浸透了少许红色果汁的传令文，发出着声音撕毁。

【――父，父亲大人！？您在做，做什么！】
【**，真以为通过公会的工作，能让你拿到传令书吗。理解力真差……这是伪信。我还以为你理解啊】

似乎很气馁啊。倒不如说，让人不禁怀疑，巴别利奇正以一种似是包含着冷笑一般的颜色的面容，继续说話。

【明明是我的女儿，你这家伙从以前开始就完全做不到这种计算。我为什么会把这次的工作交给你，似乎完全不明白啊，看你这样子】
【……那么，这不是来自于骑士团的命令，而是……阁下的意见】

卡丽娅・帕多利克垂首，声音无比的孱弱颤抖，忍受着来自父亲的冷言冷语。那想要努力不动摇的模样，如此可悲，而在身后的我看到了这种动摇。
然后，她问其言外之意。为什么。

【怎么可能会不知道。别说你不记得。打破公会禁令进入大树之森。真是个笨蛋女儿。你以为我，不，是整个帕多利克家为此做出了多大程度的奔走。希望你能把这次的事情当做一剂良药自重起来，卡丽娅・帕多利克】

悸动在胸中奔跑。血流循环到了手足的顶端，不可思议的意识。

【良药……阁下，是指，被贼人袭击之事……您知道，是这个意思吗】

巴别利奇没有回答。不，那沉默便是答案本身。
他早已知道。当然应该已经理解。袭击是在这座城堡近郊。作为城堡总督的他，对周边的治安应该是了如指掌的吧。
若是如此的話，他应该已经理解了，女儿，被野盗或者什么袭击，搞不好的話，会丢掉性命之事。背部寒气直冒。骨髓被匕首或者某种锐利刃物插入，剜心般的感觉。

【……話说在前头，卡丽娅，不要再做给家名抹黑的事。你这家伙每次做出无理之事，其影响定然波及家族。别让我再说同样的話。别再做出那种难看之事。你只要乖乖的听我说的話即可，明白吗】

巴别利奇把她看做一个成绩不好的女儿，如此自语着转过身去，似乎是在说，已经没有交谈的必要。
突然的，抬起面容。卡丽娅・帕多利克，一似乎保持着极为冷静的状态。这或许是由于她的坚强吧。可是，那果真只不过是强行贴附在表面的东西而已。
背在颤抖，身体僵硬，面部铁青。
但是，即使如此，也不能容许姿势有任何毁损。对于上位者而言，抬头，发出自己的声音，都是不被允许的，更别说是站起来。对，就像这样，直到那家伙离开为止，我们只能跪着。本应冰冷的石板感触，微妙的令人感到热意。
不可思议，不可思议的，映现在的瞳孔中的视野变得鲜明，思考变得明晰。


————————————————

理所当然的，吧。
卡丽娅・帕多利克跪在石板上，如此在心中自语道。自己无论想什么，如何挣扎，对于父亲大人而言，都只不过是碍眼而已，她充分理解了。从儿时开始，她便和其它温顺的姐姐妹妹不一样，持剑的我对于父亲大人而言一定是个异物。
不，不是的。紧咬牙关，即使想要抑制住感情，思考依然在不可思议的的巡回着。最初，注意到我。没有被赐予男孩的父亲大人，居然夸奖了宛如男孩般举止的我。啊啊，是的――――直到被赐予了男孩为止。
刚才的那番話，一定是真实的吧。卡丽娅在心底如此的理解了。我只是个，难看的女儿，也只能如此认为。即使被贼人袭击，险些丢掉性命，也只不过是一个无关紧要的存在，也只能如此认识。
自己无能的，卡丽娅・帕多利克膝盖颤抖着。想要说的話，想要传达的話，如山多。然而，对这个已经想要离开的背影，自己甚至连話都说不出来。
巴别利奇・帕多利克的手，放到了门把手上。
与之相应的，从背后传出了几乎要响彻周围的巨大叹息声。与此同时，霍然站起的巨大人影，映现在卡丽娅・帕多利克的视野一角。

