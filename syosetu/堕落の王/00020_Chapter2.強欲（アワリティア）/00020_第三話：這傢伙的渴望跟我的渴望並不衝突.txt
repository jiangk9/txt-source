「我说米蒂雅美眉啊──」
「……想打架吗？我随时奉陪。」
前线基地已经四散分布开来。
老板的军队是难得一见的优秀。身为司令官的我也很优秀，但光是一般恶魔军人的素质就很不一样了。即使是以我这个拥有大国从军经验人士的观点看来，士气也不同凡响。那战力当然也会高了。
嘻嘻嘻，这裡的老大没有欲求嘛。给出的赏赐甚至能让我这个贪婪都多少感觉到满足。那对一般恶魔来说自然是相当不错的高薪了。
大约千人的部队散布在国与国交界处的广大丘陵地带。第三军差不多都在这裡了。
跟人族的军队相比，数量是比较少，但素质可是完全不能相提并论。恶魔技能所赋予的攻击威力，就算是面对千倍的人族都还有绰绰有余。
对手是暴食及其部下，人数约为三百。嘻嘻嘻，以一支军队的兵力来说算是平均，但因魔王出战而激励的士气不容小觑，而且西卜好歹是排名第五，其军队也算是威名远播。
以军队数量看来，跟第三军相比，幾乎差了三倍。正常交战的话想必这个数量足以压倒对方，但敌方魔王的存在着实危险。暴食原本就拥有许多适合广範围歼灭的技能。魔王的技能可以触及多大的範围这点，我实在连预测都预测不出来，但一定会造成莫大的折损吧。
如何看破对方手裡的牌以及估测敌方实力，将成为胜败的关键。
嘻嘻嘻，不过，就士气这点来说，我们也不会输就是了。毕竟虽然只有一个人，但我们这边可是有平时都在守护城堡的米蒂雅·路克苏利亚哈特呢。米蒂雅相貌出色，而且最重要的是──她执掌「色慾」。
搞不好可以分一杯羹呢。士气当然会高涨了。
而那关键的米蒂雅呢，一如往常地皱着眉头，身上穿着半点情趣都没有的纯白长袍。露得不够多啊。简直像修女。
「──美眉啊，你应该要穿得更性感一点啊。这也会影响士气耶？」
对我这发自内心的忠告，美眉嗤之以鼻。
「你这是多管闲事。戴奇，虽然我已经说过很多次了……但我很讨厌别人用那样的眼光看我，讨厌到让我想吐。」
我的确已经听过很多次了。不过，这可完全不像是执掌色慾的人的发言啊。是不是哪裡搞错了？
恶魔的慾望可不是摆设。
获得职阶後，就会开启那个职业的道路，但通常不会是一条路通到底。複数的道路散成枝叶状，走哪条路是凭个人意志来决定。当然，你能使用的技能将会根據你选择了哪条路而定。
这是一种被称为技能系统树的基本概念，又称技能树（skill tree），以这方面来说，恶魔的职阶有八株技能系统树。也就是与怠惰、贪婪、色慾、愤怒、暴食、嫉妒、傲慢这些原罪一致的七株，再加上执掌基本恶魔能力的基本树，合计共八株。
这些也可说是命运的指标。只要循序渐进就能获得更加强力的技能，但恶魔的技能树，跟那种只要使用技能或累积经验值就能前进的普通职阶技能树有些不同。
恶魔的技能树，只有透过达成慾望才能窥视其深渊。这是恶魔的树当中，最初步的被动技能……也就是自动发动技能「原罪的渴望」所带来的束缚。
受到这份束缚影响，我们若只是单纯提升等级，能使用的技能是不会增加的。
满足贪慾，可以让贪婪的技能树成长，满足色慾，可以让色慾的技能树成长。
就这方面来说，美眉有些太禁欲了。
嘻嘻嘻，那样真的能让色慾树成长吗？我这个并非执掌色慾的人实在不懂就是了。
不过归根究柢，我不过是个外行啊，还是别多嘴了。我可不想因为多管闲事而惹来杀身之祸。
「嘻嘻嘻，算啦，美眉的路是美眉自己走出来的。你就尽力好好战鬥吧。」
「不用你说，奖赏我拿定了。」
「……喂喂喂，这我可不能当作没听见。派兵的可是我耶？」
就算说你很强，但竟然想一个人独占功劳，连我这个贪婪都感到惊讶。而且，你的能力……并不适合战鬥吧？
幻想魔影米蒂雅。
恶魔当中最有名的那群人是魔王，但只要成为将军级的恶魔，即使还不是魔王，也会名传千里。
她的知名度在雷西军中也算是排名前三。嘻嘻嘻，我也是在还没加入老板旗下之前就知道了喔。
不过我不懂为什麼拥有色慾（luxuria）属性的恶魔会加入怠惰恶魔的麾下就是了。
即使不适合战鬥，对手若是普通恶魔，应该也能压制杀死对方吧，但这次的对手可不是一般对手。对上恶食的魔王，实在不得不说情况棘手啊。
米蒂雅朝站在我背後的杀戮人偶快速看了一眼。
杀戮人偶虽然还没有名字，但已经拥有远超过普通恶魔的战鬥能力。只要让它装备上我收集来的稀有武器，那就真的是万夫莫敌。

由此可知术师的力量是多麼地异常。
不过，米蒂雅马上一副打从心底不感兴趣似的转向我。
「我才不要武器。也不要宝具。人偶我也不需要，更不需要地位。」
「喂喂喂，那你到底想要什麼？」
「我是色慾。所求唯色。」

原来如此啊……真有趣。
这一句话就让我清楚明白了。这傢伙所拥有的原罪，比起单纯追求物质及力量的我，罪孽深重得多。
不过，也没关係吧。这傢伙的渴望跟我的渴望并不衝突。现在我就当作是这麼一回事吧。
以商业伙伴来说，更进一步的探究……是没必要的。
「你是指我跟你的慾望并非競争关係是吗？算了，好吧。我可是信了你这句话喽？毕竟被人从背後一击作为结尾，可是最扫兴的了。」
事先警告她。
因为恶魔的军队这种东西，很容易因为意志衝突而反目成仇嘛。
嘻嘻嘻，而且米蒂雅还是对我加入军队这件事坚决反对到最後一刻的恶魔。再怎麼防备也不嫌多。
不过，以正面对决来说，她是不可能赢得了拥有魔王级武器的我就是了。
那你就好好发挥你的实力吧，可不要白白送死了啊。你想必拥有面对魔王还敢大声说要自己出战的超强实力对吧？